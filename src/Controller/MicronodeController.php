<?php

declare(strict_types=1);

namespace Drupal\micronode\Controller;

use Drupal\node\Controller\NodeController;

/**
 * Tweaks NodeController according to our needs.
 */
class MicronodeController extends NodeController {

  /**
   * {@inheritdoc}
   */
  public function addPage() {
    $build = parent::addPage();
    if (is_array($build) && !empty($build['#content'])) {
      // Remove micro-content types from the list.
      foreach (micronode_get_node_types(TRUE) as $type) {
        unset($build['#content'][$type->id()]);
      }
    }

    return $build;
  }

  /**
   * Displays add content links for available content types (microcontent only).
   *
   * @return array
   *   A render array for a list of microcontent types that can be added.
   */
  public function addMicrocontentPage() {
    $build = parent::addPage();
    if (is_array($build) && !empty($build['#content'])) {
      // Remove non-micro-content types from the list.
      foreach (micronode_get_node_types(FALSE) as $type) {
        unset($build['#content'][$type->id()]);
      }
    }

    return $build;
  }

}
