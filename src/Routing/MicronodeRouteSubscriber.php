<?php

declare(strict_types=1);

namespace Drupal\micronode\Routing;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscriber for micronode routes.
 */
class MicronodeRouteSubscriber extends RouteSubscriberBase {

  /**
   * The Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Module Handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity type manager service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The Module handler service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler) {
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if (!$this->moduleHandler->moduleExists('type_tray')) {
      // Modify the "/node/add" route to use our own controller instead.
      if ($route = $collection->get('node.add_page')) {
        $defaults = $route->getDefaults();
        $defaults['_controller'] = '\Drupal\micronode\Controller\MicronodeController::addPage';
        $route->setDefaults($defaults);
      }
    }

    // Add a new route to mimic the node add page, but only for micro-content.
    $micro_content_add_route = new Route(
      '/node/add-microcontent',
      [
        '_title' => 'Add micro-content',
        '_controller' => '\Drupal\micronode\Controller\MicronodeController::addMicrocontentPage',
      ],
      [
        '_micronode_create_any_access' => 'node',
      ],
      [
        '_node_operation_route' => TRUE,
      ]
    );
    $collection->add('micronode.add_microcontent_page', $micro_content_add_route);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents():array {
    $events = parent::getSubscribedEvents();
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', 95];
    return $events;
  }

}
