<?php

declare(strict_types=1);

namespace Drupal\micronode\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\Routing\Route;

/**
 * Defines an access checker for creating an entity of any bundle.
 */
class MicronodeCreateAnyAccessCheck implements AccessInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The key used by the routing requirement.
   *
   * @var string
   */
  protected $requirementsKey = '_micronode_create_any_access';

  /**
   * Constructs an EntityCreateAnyAccessCheck object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Checks access to create an entity of any bundle for the given route.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The parameterized route.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    $entity_type_id = $route->getRequirement($this->requirementsKey);
    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
    $access_control_handler = $this->entityTypeManager->getAccessControlHandler($entity_type_id);

    $access = AccessResult::neutral();

    /** @var \Drupal\node\NodeTypeInterface[] $bundles */
    $bundles = micronode_get_node_types(TRUE);

    // Include list cache tag as access might change if more bundles are added.
    $access->addCacheTags($this->entityTypeManager->getDefinition($entity_type->getBundleEntityType())->getListCacheTags());

    if (empty($route->getOption('_ignore_create_bundle_access'))) {
      // Check if the user is allowed to create new bundles. If so, allow
      // access, so the add page can show a link to create one.
      // @see \Drupal\Core\Entity\Controller\EntityController::addPage()
      $bundle_access_control_handler = $this->entityTypeManager->getAccessControlHandler($entity_type->getBundleEntityType());
      $access = $access->orIf($bundle_access_control_handler->createAccess(NULL, $account, [], TRUE));
      if ($access->isAllowed()) {
        return $access;
      }
    }

    // Check whether an entity of any micronode bundle may be created.
    foreach ($bundles as $bundle) {
      $bundle_access = $access_control_handler->createAccess($bundle->id(), $account, [], TRUE);
      $access->inheritCacheability($bundle_access);

      // Allow access if the user can create at least one micronode bundle.
      if ($bundle_access->isAllowed()) {
        return AccessResult::allowed()->inheritCacheability($access);
      }
    }

    return $access;
  }

}
