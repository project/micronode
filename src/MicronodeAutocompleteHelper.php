<?php

declare(strict_types=1);

namespace Drupal\micronode;

/**
 * Autocomplete helper methods.
 */
class MicronodeAutocompleteHelper {

  /**
   * Disallow micronodes from entity_autocomplete.
   *
   * This method sets target_bundles if they have not already been set. One
   * can still allow micronodes in special cases as needed and this method
   * will NOT override that.
   *
   * @param array $element
   *   The element array to be processed.
   * @return array
   *   The same element, after being processed
   */
  public static function disallowMicronodes(array $element): array {
    if (isset($element['#target_type']) && $element['#target_type'] === 'node') {
      if (!isset($element['#selection_settings']['target_bundles'])) {
        $element['#selection_settings']['target_bundles'] = [];
        $types = array_keys(micronode_get_node_types(FALSE));
        foreach($types as $type) {
          $element['#selection_settings']['target_bundles'][$type] = $type;
        }
      }
    }
    return $element;
  }

}
