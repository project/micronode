<?php

declare(strict_types=1);

namespace Drupal\micronode\Plugin\views\wizard;

use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Plugin\views\wizard\Node;

/**
 * Micronode views wizard class.
 *
 * This class is not a plugin per se, but it is used instead of the class for
 * the plugin defined in \Drupal\node\Plugin\views\wizard\Node, since we want
 * to customize its behavior, while keeping its plugin id.
 *
 * @see micronode_views_plugins_wizard_alter()
 */
class Micronode extends Node {

  /**
   * {@inheritdoc}
   */
  protected function defaultDisplayFilters($form, FormStateInterface $form_state) {
    $filters = parent::defaultDisplayFilters($form, $form_state);

    // We want to have a default behavior that node entities marked as
    // "micro-content" should not show up in any newly created view,
    // out-of-the-box. In order to accomplish that, we add this filter here,
    // so if the site builder really wants to show micro-content in the view
    // they are creating on the UI, they need to manually remove this filter.
    $filters['micronode_is_microcontent'] = [
      'value' => FALSE,
      'table' => 'node_field_data',
      'field' => 'micronode_is_microcontent',
      'plugin_id' => 'micronode_is_microcontent',
      'entity_type' => 'node',
      'entity_field' => 'micronode_is_microcontent',
      'id' => 'micronode_is_microcontent',
      'expose' => [
        'operator' => FALSE,
      ],
      'group' => 1,
    ];

    return $filters;
  }

}
