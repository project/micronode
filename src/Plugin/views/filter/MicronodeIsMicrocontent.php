<?php

declare(strict_types=1);

namespace Drupal\micronode\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\BooleanOperator;

/**
 * Detects whether a node is micro-content.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("micronode_is_microcontent")
 */
class MicronodeIsMicrocontent extends BooleanOperator {

  /**
   * {@inheritdoc}
   */
  public function operators() {
    return [
      '=' => [
        'title' => $this->t('Is equal to'),
        'method' => 'queryOpBoolean',
        'short' => $this->t('='),
        'values' => 1,
        'query_operator' => self::EQUAL,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return ['config:node_type_list'];
  }

  /**
   * {@inheritdoc}
   */
  public function getValueOptions() {
    $this->valueOptions = [0 => $this->t('Exclude Micro-content'), 1 => $this->t('Restrict to Micro-content')];
    return $this->valueOptions;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();
    $micronode_bundles = array_keys(micronode_get_node_types(TRUE));
    if ($this->value) {
      // Restrict to micronodes.
      if ($micronode_bundles) {
        $this->query->addWhere($this->options['group'], "$this->tableAlias.type", $micronode_bundles, 'IN');
      }
      else {
        // If there are no micronode bundles views should return nothing.
        // Modeled after Drupal\Core\Database\Query::alwaysFalse().
        $this->query->addWhereExpression($this->options['group'], '1 = 0');
      }
    }
    else {
      // Exclude micronodes.
      if ($micronode_bundles) {
        $this->query->addWhere($this->options['group'], "$this->tableAlias.type", $micronode_bundles, 'NOT IN');
      }
    }
  }

}