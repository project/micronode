# Nodes as Micro-Content (micronode)

## Overview

This module allows to mark nodes as "micro-content". As a result, nodes of
this type will no longer be accessible on their standalone page by anonymous
visitors (while editors still can visit them when logged in).

In addition, a few other adjustments are made:
- Menu links are now separate ("Add Content" includes only normal nodes, while
"Add Micro-Content" will include links to all micro-content types)
- The "Add Content" page will not display types marked as micro-content
- The "Add Micro-Content" page will only display micro-content types
- When creating a new View, by default micro-content nodes will not be included.

Why yet-another module? Isn't https://www.drupal.org/project/rabbit_hole or
https://www.drupal.org/project/microcontent enough to solve the same problem?

These modules have slightly different use cases and configuration.

On one hand, Rabbit Hole focuses on hiding/denying access to nodes of a given
type. It doesn't re-organize menu links, and it's complex to take these nodes
out of views lists, for example (admins need to remember to modify the view
any time the content type is marked as hidden).

On the other hand, entities provided by the Microcontent module work well for
their purpose, but they are not Nodes :). With this (micronode) module,
microcontent items are nodes because in some circumstances it might be
desirable to have all content entities of the same type. Having them as
different entities also makes it more complex to "promote" a micro-content
to a normal node type, if that ends up being a need as the project matures.

With this module any content type can be marked as micro-content at any point,
and vice-versa.

## Configuration

After installing the module, edit a content type (for example
`/admin/structure/types/manage/article` if your type is named `article`),
and click the "Micro-content settings" vertical tab to mark nodes of this type
as micro-content.

Do the same for every content type that should behave as micro-content.

Note: If you are installing this module on a site with existing node types, and
you want to use the Views filter "Is Micro-content" provided by this module,
you need to re-save all your existing content types to have the flag properly
initialized, regardless if they are marked as micro-content or not. Views will
only recognize nodes whose types have this flag at either TRUE or FALSE, but
on existing types this will be NULL unless they are re-saved.
