<?php

declare(strict_types=1);

// @codingStandardsIgnoreStart

namespace Drupal\Tests\micronode\Functional;

use Drupal\Core\Session\AnonymousUserSession;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests basic functionality.
 *
 * @group micronode
 */
class MicronodeFunctionalTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block',
    'node',
    'micronode',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * An admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * An editor user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $editorUser;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Place some blocks to make our lives easier down the road.
    $this->drupalPlaceBlock('system_breadcrumb_block');
    $this->drupalPlaceBlock('local_tasks_block');
    $this->drupalPlaceBlock('local_actions_block');
    $this->drupalPlaceBlock('page_title_block');

    $this->adminUser = $this->drupalCreateUser([
      'administer content types',
      'administer nodes',
      'bypass node access',
    ]);

  }

  /**
   * Tests that micro-content can be turned on/off, and that controls access.
   */
  public function testMicroContentAccess() {
    $assert_session = $this->assertSession();

    $type1 = $this->drupalCreateContentType([
      'type' => 'one',
      'name' => 'Type One',
    ]);
    // By default our flag is off.
    $is_microcontent = $type1->getThirdPartySetting('micronode', 'micronode_is_microcontent');
    $this->assertEmpty($is_microcontent);

    // Create a node so we can test its access for anonymous users.
    $node1 = $this->drupalCreateNode([
      'type' => 'one',
      'title' => 'First node',
      'status' => TRUE,
    ]);
    // Visit the node as anonymous, to make sure the access check is cached.
    $this->drupalGet($node1->toUrl());
    $assert_session->statusCodeEquals(200);

    $this->drupalLogin($this->adminUser);

    // The flag can be set through the UI.
    $this->drupalGet('/admin/structure/types/manage/one');
    $assert_session->elementExists('css', 'input[name="micronode[micronode_is_microcontent]"]')
      ->check();
    $assert_session->elementExists('css', 'input#edit-submit')
      ->click();
    $assert_session->pageTextContains("The content type {$type1->label()} has been updated");
    $type1 = \Drupal::entityTypeManager()->getStorage('node_type')
      ->loadUnchanged('one');
    $is_microcontent = $type1->getThirdPartySetting('micronode', 'micronode_is_microcontent', NULL);
    $this->assertTrue($is_microcontent);

    // Log out and verify now anonymous visitors no longer have access.
    $this->drupalLogout();
    $this->drupalGet($node1->toUrl());
    $assert_session->statusCodeEquals(403);

    // However, access to the same node when rendered not at its own URL is OK.
    // @todo

    // A programatic call to the same node is also OK for anonymous users.
    $anonymous = new AnonymousUserSession();
    $this->assertTrue($node1->access('view', $anonymous));

    // Uncheck the micro-content flag and standalone access is now restored.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/structure/types/manage/one');
    $assert_session->elementExists('css', 'input[name="micronode[micronode_is_microcontent]"]')
      ->uncheck();
    $assert_session->elementExists('css', 'input#edit-submit')
      ->click();
    $assert_session->pageTextContains("The content type {$type1->label()} has been updated");
    $type1 = \Drupal::entityTypeManager()->getStorage('node_type')
      ->loadUnchanged('one');
    $is_microcontent = $type1->getThirdPartySetting('micronode', 'micronode_is_microcontent', NULL);
    $this->assertFalse($is_microcontent);
    $this->drupalLogout();
    $this->drupalGet($node1->toUrl());
    $assert_session->statusCodeEquals(200);
  }

  /**
   * Tests that micro-content is split off from the content->add page.
   */
  public function testMicroContentAddPages() {
    $assert_session = $this->assertSession();

    $type1 = $this->drupalCreateContentType([
      'type' => 'one',
      'name' => 'Type One',
    ]);
    $type2 = $this->drupalCreateContentType([
      'type' => 'two',
      'name' => 'Type Two',
    ]);
    $type3 = $this->drupalCreateContentType([
      'type' => 'three',
      'name' => 'Type Three',
    ]);

    $this->drupalLogin($this->adminUser);

    // Visit /node/add and verify all types are listed there.
    $this->drupalGet('/node/add');
    $assert_session->elementTextContains('css', 'h1', 'Add content');
    $assert_session->pageTextContains('Type One');
    $assert_session->pageTextContains('Type Two');
    $assert_session->pageTextContains('Type Three');

    // The add micro-content page contains nothing at this point.
    $this->drupalGet('/node/add-microcontent');
    $assert_session->elementTextContains('css', 'h1', 'Add Micro-content');
    $assert_session->pageTextNotContains('Type One');
    $assert_session->pageTextNotContains('Type Two');
    $assert_session->pageTextNotContains('Type Three');

    // Mark Type2 as micro-content and see what changes.
    $this->drupalGet('/admin/structure/types/manage/two');
    $assert_session->elementExists('css', 'input[name="micronode[micronode_is_microcontent]"]')
      ->check();
    $assert_session->elementExists('css', 'input#edit-submit')
      ->click();
    $assert_session->pageTextContains("The content type {$type2->label()} has been updated");
    $type2 = \Drupal::entityTypeManager()->getStorage('node_type')
      ->loadUnchanged('two');
    $is_microcontent = $type2->getThirdPartySetting('micronode', 'micronode_is_microcontent', NULL);
    $this->assertTrue($is_microcontent);

    $this->drupalGet('/node/add');
    $assert_session->elementTextContains('css', 'h1', 'Add content');
    $assert_session->pageTextContains('Type One');
    $assert_session->pageTextNotContains('Type Two');
    $assert_session->pageTextContains('Type Three');

    $this->drupalGet('/node/add-microcontent');
    $assert_session->elementTextContains('css', 'h1', 'Add Micro-content');
    $assert_session->pageTextNotContains('Type One');
    $assert_session->pageTextContains('Type Two');
    $assert_session->pageTextNotContains('Type Three');

    // Test micronode_get_node_types() function.
    $all_types = micronode_get_node_types();
    $this->assertEqualsCanonicalizing(['one', 'three', 'two'], array_keys($all_types));
    $micronode_types = micronode_get_node_types(TRUE);
    $this->assertEqualsCanonicalizing(['two'], array_keys($micronode_types));
    $macronode_types = micronode_get_node_types(FALSE);
    $this->assertEqualsCanonicalizing(['one', 'three'], array_keys($macronode_types));
  }

}
