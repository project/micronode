<?php

declare(strict_types=1);

// @codingStandardsIgnoreStart

namespace Drupal\Tests\micronode\Functional;

use Drupal\Core\Session\AnonymousUserSession;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests views integration.
 *
 * @group micronode
 */
class MicronodeViewsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block',
    'node',
    'micronode',
    'micronode_views_test',
    'system',
    'views',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * An admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * An editor user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $editorUser;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Place some blocks to make our lives easier down the road.
    $this->drupalPlaceBlock('system_breadcrumb_block');
    $this->drupalPlaceBlock('local_tasks_block');
    $this->drupalPlaceBlock('local_actions_block');
    $this->drupalPlaceBlock('page_title_block');

    $this->adminUser = $this->drupalCreateUser([
      'administer content types',
      'administer nodes',
      'bypass node access',
    ]);

  }

  /**
   * Tests that the views filter plugin works as expected.
   */
  public function testViewsMicrocontentFilter() {
    $assert_session = $this->assertSession();


    // Our test module created two content types and two views.
    // Generate a few nodes here so that can be tested when visiting them.
    $node1 = $this->drupalCreateNode([
      'type' => 'one',
      'title' => 'First node',
      'status' => TRUE,
    ]);
    $node2 = $this->drupalCreateNode([
      'type' => 'one',
      'title' => 'Second node',
      'status' => TRUE,
    ]);
    $node3 = $this->drupalCreateNode([
      'type' => 'two',
      'title' => 'Third node',
      'status' => TRUE,
    ]);
    $node4 = $this->drupalCreateNode([
      'type' => 'two',
      'title' => 'Fourth node',
      'status' => TRUE,
    ]);

    // As anonymous visit both views and assert their contents. Initially none
    // of the types is flagged as micro-content.
    $this->drupalGet('/content-only');
    $assert_session->elementTextContains('css', 'h1', 'Content only');
    $assert_session->pageTextContains('First node');
    $assert_session->pageTextContains('Second node');
    $assert_session->pageTextContains('Third node');
    $assert_session->pageTextContains('Fourth node');
    $assert_session->optionExists('type', 'All');
    $assert_session->optionExists('type', 'one');
    $assert_session->optionExists('type', 'two');
    $this->drupalGet('/microcontent-only');
    $assert_session->elementTextContains('css', 'h1', 'Microcontent only');
    $assert_session->pageTextNotContains('First node');
    $assert_session->pageTextNotContains('Second node');
    $assert_session->pageTextNotContains('Third node');
    $assert_session->pageTextNotContains('Fourth node');
    $assert_session->optionExists('type', 'All');
    $assert_session->optionNotExists('type', 'one');
    $assert_session->optionNotExists('type', 'two');

    // The same is true when logged in (this also serves to warm caches).
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/content-only');
    $assert_session->elementTextContains('css', 'h1', 'Content only');
    $assert_session->pageTextContains('First node');
    $assert_session->pageTextContains('Second node');
    $assert_session->pageTextContains('Third node');
    $assert_session->pageTextContains('Fourth node');
    $assert_session->optionExists('type', 'All');
    $assert_session->optionExists('type', 'one');
    $assert_session->optionExists('type', 'two');
    $this->drupalGet('/microcontent-only');
    $assert_session->elementTextContains('css', 'h1', 'Microcontent only');
    $assert_session->pageTextNotContains('First node');
    $assert_session->pageTextNotContains('Second node');
    $assert_session->pageTextNotContains('Third node');
    $assert_session->pageTextNotContains('Fourth node');
    $assert_session->optionExists('type', 'All');
    $assert_session->optionNotExists('type', 'one');
    $assert_session->optionNotExists('type', 'two');

    // Mark type one as micro-content and check again.
    $this->drupalGet('/admin/structure/types/manage/one');
    $assert_session->elementExists('css', 'input[name="micronode[micronode_is_microcontent]"]')
      ->check();
    $assert_session->elementExists('css', 'input#edit-submit')
      ->click();
    $assert_session->pageTextContains("The content type One has been updated");
    $type1 = \Drupal::entityTypeManager()->getStorage('node_type')
      ->loadUnchanged('one');
    $is_microcontent = $type1->getThirdPartySetting('micronode', 'micronode_is_microcontent', NULL);
    $this->assertTrue($is_microcontent);

    $this->drupalLogout();

    $this->drupalGet('/content-only');
    $assert_session->elementTextContains('css', 'h1', 'Content only');
    $assert_session->pageTextNotContains('First node');
    $assert_session->pageTextNotContains('Second node');
    $assert_session->pageTextContains('Third node');
    $assert_session->pageTextContains('Fourth node');
    $assert_session->optionExists('type', 'All');
    $assert_session->optionNotExists('type', 'one');
    $assert_session->optionExists('type', 'two');
    $this->drupalGet('/microcontent-only');
    $assert_session->elementTextContains('css', 'h1', 'Microcontent only');
    $assert_session->pageTextContains('First node');
    $assert_session->pageTextContains('Second node');
    $assert_session->pageTextNotContains('Third node');
    $assert_session->pageTextNotContains('Fourth node');
    $assert_session->optionExists('type', 'All');
    $assert_session->optionExists('type', 'one');
    $assert_session->optionNotExists('type', 'two');

    $this->drupalLogin($this->adminUser);

    $this->drupalGet('/content-only');
    $assert_session->elementTextContains('css', 'h1', 'Content only');
    $assert_session->pageTextNotContains('First node');
    $assert_session->pageTextNotContains('Second node');
    $assert_session->pageTextContains('Third node');
    $assert_session->pageTextContains('Fourth node');
    $assert_session->optionExists('type', 'All');
    $assert_session->optionNotExists('type', 'one');
    $assert_session->optionExists('type', 'two');
    $this->drupalGet('/microcontent-only');
    $assert_session->elementTextContains('css', 'h1', 'Microcontent only');
    $assert_session->pageTextContains('First node');
    $assert_session->pageTextContains('Second node');
    $assert_session->pageTextNotContains('Third node');
    $assert_session->pageTextNotContains('Fourth node');
    $assert_session->optionExists('type', 'All');
    $assert_session->optionExists('type', 'one');
    $assert_session->optionNotExists('type', 'two');

    // Confirm that on a view without the micronode_is_microcontent filter
    // the content type form is not altered.
    $this->drupalGet('/all-content');
    $assert_session->elementTextContains('css', 'h1', 'All content');
    $assert_session->pageTextContains('First node');
    $assert_session->pageTextContains('Second node');
    $assert_session->pageTextContains('Third node');
    $assert_session->pageTextContains('Fourth node');
    $assert_session->optionExists('type', 'All');
    $assert_session->optionExists('type', 'one');
    $assert_session->optionExists('type', 'two');
  }

}
