<?php

namespace Drupal\Tests\micronode\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Tests views widget code.
 *
 * @group micronode
 */
// @codingStandardsIgnoreFile
class MicronodeViewsWidgetTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block',
    'node',
    'micronode',
    'system',
    'views',
    'views_ui',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * An admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * An editor user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $editorUser;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Place some blocks to make our lives easier down the road.
    $this->drupalPlaceBlock('system_breadcrumb_block');
    $this->drupalPlaceBlock('local_tasks_block');
    $this->drupalPlaceBlock('local_actions_block');
    $this->drupalPlaceBlock('page_title_block');

    $this->adminUser = $this->drupalCreateUser(array(
      'administer content types',
      'administer nodes',
      'bypass node access',
      'administer views',
    ));
  }

  /**
   * Tests that our views wizard does what we expect it to.
   */
  public function testViewsWizard() {
    $assert_session = $this->assertSession();
    $session = $this->getSession();
    $page = $session->getPage();

    $this->drupalLogin($this->adminUser);

    $this->drupalGet('/admin/structure/views/add');
    // Give the view a name. Make sure the machine name JS doesn't mess with us.
    $assert_session->elementExists('css', '#edit-label')
      ->setValue('Foo');
    $session->wait(5000, "jQuery('.machine-name-value').text() === 'foo'");
    // Make sure this is a view of nodes.
    $page->selectFieldOption('show[wizard_key]', 'node');
    // Create a page display.
    $assert_session->elementExists('css', '#edit-page-create')
      ->check();
    // All good, save the view.
    $page->pressButton('Save and edit');
    $assert_session->pageTextContains('The view Foo has been saved');
    // Verify the view has a filter that excludes Micro-content.
    $assert_session->elementTextContains('css', 'div[data-drupal-selector="edit-displays-settings-settings-content-tab-content-details-columns-first-filters"]', 'Content: Is Micro-content (= Exclude Micro-content)');
  }

}
