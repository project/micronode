<?php

declare(strict_types=1);

/**
 * @file
 * Views hook implementations for micronode.module.
 */

/**
 * Implements hook_views_data_alter().
 */
function micronode_views_data_alter(&$data) {
  $data['node_field_data']['micronode_is_microcontent'] = [
    'title' => t('Is Micro-content'),
    'group' => t('Content'),
    'filter' => [
      'value' => FALSE,
      'title' => t('Is Micro-content'),
      'help' => t('Checks whether this node is marked as micro-content.'),
      'type' => 'yes-no',
      'field' => 'type',
      'id' => 'micronode_is_microcontent',
    ],
  ];
}
